#include "String.h"

// Constructs a new String object
String::String(std::string value)
{
	this->_value = value;
}

// Returns true - string is printable
bool String::IsPrintable() const
{
	return true;
}

// Returns a printable string of the object
std::string String::toString() const
{
	return this->_value;
}
