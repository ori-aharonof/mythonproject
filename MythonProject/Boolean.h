#ifndef BOOLEAN_H
#define BOOLEAN_H
#include "type.h"
#include "Helper.h"

class Boolean : public Type
{
	bool _value;
public:
	Boolean(const std::string& value);
	bool IsPrintable() const;
	std::string toString() const;
};

#endif // BOOLEAN_H