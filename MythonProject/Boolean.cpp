#include "Boolean.h"

// Constructs a new Boolean object
Boolean::Boolean(const std::string& value)
{
	this->_value = Helper::stringToBool(value);
}

// Returns true (bool is printable)
bool Boolean::IsPrintable() const
{
	return true;
}

// Returns a printable string of the object
std::string Boolean::toString() const
{
	return this->_value ? "True" : "False";
}
