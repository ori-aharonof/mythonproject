#ifndef STRING_H
#define STRING_H
#include "type.h"
#include <string>

class String : public Type
{
	std::string _value;
	
public:
	String(std::string value);
	bool IsPrintable() const;
	std::string toString() const;
};

#endif // STRING_H