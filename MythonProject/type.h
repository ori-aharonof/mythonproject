#ifndef TYPE_H
#define TYPE_H

#include <iostream>
#include <string>

class Type
{
	bool _is_temp;
public:
	Type();
	bool IsTemp();
	void set_is_temp(bool value);
	virtual bool IsPrintable() const = 0;
	virtual std::string toString() const = 0;
};
#endif //TYPE_H
