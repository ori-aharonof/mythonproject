#pragma once
#ifndef VOID_H
#define VOID_H
#include "type.h"

class Void : public Type
{
	
public:
	Void();
	bool IsPrintable() const;
	std::string toString() const;
};

#endif // VOID_H