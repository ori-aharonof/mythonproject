#include "Integer.h"

// constructor method
Integer::Integer(const std::string& str)
{
	// assigning the string as an int 
	this->_value = std::stoi(str);
}

// Returns true (int is printable)
bool Integer::IsPrintable() const
{
	return true;
}

// Returns a printable string of the object
std::string Integer::toString() const
{
	return std::to_string(this->_value);
}