#include "NameErrorException.h"

NameErrorException::NameErrorException(std::string name)
{
    this->_name = name;
}

const char* NameErrorException::what() const throw()
{
    std::string excep = "";
    excep += "NameError : name ";
    excep += this->_name;
    excep += " is not defined";
    const char* temp = excep.c_str();
    return temp;
}
