#include "parser.h"

std::unordered_map<std::string, Type*> Parser::_variables;

// Parses the given string into a commant / an object.
// Input: the command the user typed in the interpreter
Type* Parser::parseString(std::string str) throw()
{
	// ensuring that the string isnt empty and doesnt start with a tab or a space
	if (str[0] == '\t' || str[0] == ' ')
	{
		throw IndentationException();
	}
	else if (str.length() > 0)
	{
		Type* returning_obj = getVariableValue(str);
		// in case the given string is not a known identifier
		if (returning_obj == nullptr)
		{
			returning_obj = getType(str);
			if (returning_obj == nullptr)
			{
				makeAssignment(str);
				//throw SyntaxException();
			}
			else
			{
				returning_obj->set_is_temp(true);
				if (returning_obj->IsPrintable())
				{
					// printing the object
					std::cout << returning_obj->toString() << "\n";
				}
				return returning_obj;
			}
		}
		else
		{
			std::cout << (returning_obj->IsPrintable() ? returning_obj->toString() : "") << "\n";
			return returning_obj;
		}
	}
}

// Analizes the string and returns an object with the right type.
// For example - returns a new Integer for the string "1235".
// Input: the string to build the object from. Output: the new object
Type* Parser::getType(std::string& str)
{
	if (Helper::isInteger(str))
	{
		Helper::removeLeadingZeros(str);
		return new Integer(str);
	}
	else if (Helper::isBoolean(str))
	{
		return new Boolean(str);
	}
	else if (Helper::isString(str))
	{
		return new String(str);
	}
	return nullptr;
}

// Checks if the given Variable identifier name is legal.
// Input: the name to check.
// Output: true if the name is legal, overwise false.
bool Parser::isLegalVarName(const std::string& str)
{
	// if the first letter is not a digit
	if (!Helper::isDigit(str[0]))
	{
		int i = 1;
		
		for (i = 1; i < str.length(); i++)
		{
			// in case the current char on the string is not a letter or an underscore
			if (!(Helper::isLetter(str[i]) || Helper::isUnderscore(str[i])))
			{
				return false;
			}
		}
		return true;
	}
	// in case the first letter is a digit
	else
	{
		return false;
	}
}

bool Parser::makeAssignment(const std::string& str)
{
	Type* value_obj = nullptr;
	std::string name = "", value = "";
	int assignment_index = str.find('=');

	// if the string contains '=' char and it's not the last char
	if (assignment_index != std::string::npos && assignment_index != str.length() - 1)
	{
		name = str.substr(0, assignment_index);
		// removing spaces between the name and the operator
		Helper::rtrim(name);
		if (isLegalVarName(name))
		{
			value = str.substr(assignment_index + 1, str.length());
			// removing spaces at the start and the end of the value
			Helper::trim(value);
			value_obj = getType(value);
			if (value_obj != nullptr)
			{
				_variables[name] = value_obj;
				return true;
			}
			// in case the value is invalid
			else
			{
				throw SyntaxException();
			}
		}
		// in case the name is illegal
		else
		{
			throw NameErrorException(name);
		}
	}
	else
	{
		throw SyntaxException();
	}
}

// Returns the value of the given indentifier name
Type* Parser::getVariableValue(const std::string& str)
{
	return _variables[str];
}
//// Deletes all variables
//void Parser::clearMemory()
//{
//	std::iterator<std::unordered_map>
//}
