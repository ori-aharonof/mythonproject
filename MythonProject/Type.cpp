#include "type.h"

// Constructor
Type::Type()
{
	this->_is_temp = false;
}

bool Type::IsTemp()
{
	return false;
}

void Type::set_is_temp(bool value)
{
	this->_is_temp = value;
}
