#ifndef INTEGER_H
#define INTEGER_H

#include "type.h"
#include <string>

class Integer : public Type
{
	int _value;
public:
	Integer(const std::string& value);
	bool IsPrintable() const;
	std::string toString() const;
};

#endif // INTEGER_H