#ifndef PARSER_H
#define PARSER_H

#include "InterperterException.h"
#include "IndentationException.h"
#include "SyntaxException.h"
#include "NameErrorException.h"
#include "type.h"
#include "Helper.h"
#include "Integer.h"
#include "Boolean.h"
#include "String.h"
#include <string>
#include <unordered_map>
#include <iterator>
#include <iostream>
#include <sstream>

class Parser
{
	static std::unordered_map<std::string, Type*> _variables;
public:
	static Type* parseString(std::string str) throw();
	static Type* getType(std::string &str);

private:

	static bool isLegalVarName(const std::string& str);
	static bool makeAssignment(const std::string& str);
	static Type* getVariableValue(const std::string &str);
	//static void clearMemory();
};

#endif //PARSER_H
