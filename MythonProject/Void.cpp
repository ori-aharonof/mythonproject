#include "Void.h"

// Constructor
Void::Void()
{

}

// A void object cannot be printed
bool Void::IsPrintable() const
{
	return false;
}

// Returns an empty string (void)
std::string Void::toString() const
{
	return "";
}
